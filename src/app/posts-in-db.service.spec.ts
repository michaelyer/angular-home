import { TestBed } from '@angular/core/testing';

import { PostsInDBService } from './posts-in-db.service';

describe('PostsInDBService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PostsInDBService = TestBed.get(PostsInDBService);
    expect(service).toBeTruthy();
  });
});
