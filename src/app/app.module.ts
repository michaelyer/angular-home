import { PostsInDBService } from './posts-in-db.service';
import { BooksService } from './books.service';
import { PostsService } from './posts.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AppComponent } from './app.component';
import { BooksComponent } from './books/books.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TopbarComponent } from './topbar/topbar.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import { RouterModule, Routes } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AngularFirestoreModule} from '@angular/fire/firestore';
import { AddBookComponent } from './add-book/add-book.component'; 
import { FormsModule } from '@angular/forms'; 
import {MatInputModule} from '@angular/material/input';
import { EditBookComponent } from './edit-book/edit-book.component';
import { PostsComponent } from './posts/posts.component';
import { HttpClientModule } from '@angular/common/http';
import { PostInDBComponent } from './post-in-db/post-in-db.component';
import { AddPostsComponent } from './add-posts/add-posts.component';

const appRoutes: Routes = [
  {path: '',
    redirectTo:'/books',
    pathMatch: 'full'
  }, 
  {path: 'add-book', component: AddBookComponent},
  {path: 'books', component: BooksComponent},
  {path: 'edit-book/:id/:title/:author', component: EditBookComponent},
  {path: 'posts', component: PostsComponent},
  {path: 'add-posts', component: AddPostsComponent},
  {path: 'post-in-db', component: PostInDBComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    TopbarComponent,
    AddBookComponent,
    EditBookComponent,
    PostsComponent,
    PostInDBComponent,
    AddPostsComponent
  ],
  imports: [
    BrowserModule,
    MatInputModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true }
    )
  ],
  providers: [PostsService,BooksService,PostsInDBService],
  bootstrap: [AppComponent]
})
export class AppModule { }
