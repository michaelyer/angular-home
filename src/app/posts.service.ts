import { map } from 'rxjs/operators';
import { Posts } from './posts';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Users } from './users';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private POSTURL = "https://jsonplaceholder.typicode.com/posts";
  private USERS = "https://jsonplaceholder.typicode.com/users";
  private itemsCollection: AngularFirestoreCollection<Posts>; 


  getPostsData(): Observable<Posts[]>{
    return this.http.get<Posts[]>(`${this.POSTURL}`).pipe(
      map((data)=>this.addUsersToPosts(data))
    )

  }

  addPostToDB(){
    const users = this.getUserData(); 
    const posts = this.http.get<Posts[]>(`${this.POSTURL}`);
    let items: Observable<Posts[]>; 
    items = this.itemsCollection.valueChanges(); 
    items.forEach(data => {
      if (data.length === 0){
        posts.forEach(post =>{
          post.forEach(p =>{
            users.forEach(user =>{
              user.forEach(u => {
                if (u.id === p.userId){
                  p.username = u.name;
               this.db.collection('Posts').add({
                 title: p.title,
                 body: p.body,
                 username: p.username

               }) } 
              })
            })
          })
        })
      }
      else{
        console.log("DB is already updated");
      }
    })
  }

  getUserData(): Observable<Users[]>{
    return this.http.get<Users[]>(`${this.USERS}`);
  }

  addUsersToPosts(data: Posts[]): Posts[]{
   const users = this.getUserData();
   const postsArray = []; 
   users.forEach(user => {
     user.forEach(u => {
       data.forEach(post =>{
         if(post.userId === u.id){
           postsArray.push({
             id: post.id,
             userId: post.userId, 
             title: post.title, 
             body: post.body, 
             username: u.name


           })
         }
       })
     })
   }) 
   return postsArray; 
  }
    

  constructor(private http: HttpClient, private db: AngularFirestore) { 
    this.itemsCollection = db.collection<Posts>('Posts');
  }
}
