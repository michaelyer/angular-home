import { Component, OnInit } from '@angular/core';
import { PostsInDBService } from '../posts-in-db.service';

@Component({
  selector: 'app-post-in-db',
  templateUrl: './post-in-db.component.html',
  styleUrls: ['./post-in-db.component.css']
})
export class PostInDBComponent implements OnInit {

  posts$;
  
  panelOpenState = false; 


  constructor(private postsindbservice: PostsInDBService) { }

  ngOnInit() {
    this.posts$ = this.postsindbservice.getPosts();
  }

}
