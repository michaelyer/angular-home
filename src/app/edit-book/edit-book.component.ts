import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent implements OnInit {

  id:any;
  title: string; 
  author: string;
  Etitle = new FormControl("", [Validators.required]); 
  Eauthor = new FormControl("", [Validators.required]); 

  getTitleErrorMessage(){
    return this.Etitle.hasError('required') ? 'You Must Enter A Title' : ''; 
  }

  getAuthorErrorMessage(){
    return this.Eauthor.hasError('required') ? 'You Must Enter An Author' : ''; 
  }


  constructor(private router: Router, private booksservice: BooksService, private route: ActivatedRoute) {

   }


  editBook(){
    this.booksservice.editBook(this.id, this.title, this.author); 
    this.router.navigate(['/books']);
  }

  ngOnInit() {

    this.title = this.route.snapshot.params['title'];
    this.author = this.route.snapshot.params['author'];
    this.id = this.route.snapshot.params['id'];

  }

}
