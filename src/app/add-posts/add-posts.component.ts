import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { PostsInDBService } from '../posts-in-db.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-posts',
  templateUrl: './add-posts.component.html',
  styleUrls: ['./add-posts.component.css']
})
export class AddPostsComponent implements OnInit {

  title: string
  body: string
  username: string

  addPost(){
    this.postsindbservice.addPost(this.title, this.body,this.username);
    this.router.navigate(['/post-in-db']);
  }

  constructor(private postsindbservice: PostsInDBService,private router: Router) { }

  ngOnInit() {
  }

}
