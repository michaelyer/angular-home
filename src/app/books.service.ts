import { Observable } from 'rxjs';
import { Books } from './books';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { identifierModuleUrl } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  book:Books
  add_book(title:string, author:string){
    this.book = {
      title: title,
      author: author
    } 
    this.db.collection('Books').add(this.book);
  }

  get_books(): Observable<any[]> {
    return this.db.collection('Books').valueChanges({idField: 'id'});
  }

  editBook(id:any, title:string, author:string){
    return this.db.collection('Books').doc(id).update({
      title: title,
      author: author
    })
  }

  deleteBook(id:any){
    this.db.collection('Books').doc(id).delete(); 
  }

 
  constructor(private db:AngularFirestore) { }
}
