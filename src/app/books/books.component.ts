import { BooksService } from './../books.service';
import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  @Output() panelOpenState = false; 

  // books = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]

  books$; 
  

  deleteBook(id:any){
    this.booksservice.deleteBook(id); 
  }


  constructor(private booksservice: BooksService) { }

  ngOnInit() {
    this.books$ = this.booksservice.get_books(); 
  }

}
