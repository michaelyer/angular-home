import { Title } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { Posts } from './posts';

@Injectable({
  providedIn: 'root'
})
export class PostsInDBService {

  post:Posts; 

  getPosts(): Observable<any[]>{
    return this.db.collection('Posts').valueChanges({idField: 'id'});
  }

  addPost(title:string, body:string, username: string){
   this.db.collection("Posts").add({
     title: title, 
     body: body, 
     username: username
   })
  }

  constructor(private db: AngularFirestore) { }
}
