import { PostsService } from './../posts.service';
import { Posts } from './../posts';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false; 
  posts$: Observable<Posts[]>;

  addToDB(){
    this.postservice.addPostToDB();
  }

  constructor(private postservice: PostsService) { }

  ngOnInit() {
    this.posts$ = this.postservice.getPostsData();

  }

}
