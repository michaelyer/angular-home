import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {
  title:string;
  author:string;

  constructor(private router: Router, private booksservice: BooksService) {
   }

  ngOnInit() {
  }

  addBooks(){
    this.booksservice.add_book(this.title, this.author);
    this.router.navigate(['/books']);
  }
}
