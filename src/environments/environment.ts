// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false, 
   firebaseConfig : {
    apiKey: "AIzaSyDf9jKQKtRDOJmPpdEqAcuxXTtqc01mi3g",
    authDomain: "angular-home-project-ef276.firebaseapp.com",
    databaseURL: "https://angular-home-project-ef276.firebaseio.com",
    projectId: "angular-home-project-ef276",
    storageBucket: "angular-home-project-ef276.appspot.com",
    messagingSenderId: "396592383730",
    appId: "1:396592383730:web:f1be07d821589b0c56726b",
    measurementId: "G-JV05DFMDQ0"
   }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
